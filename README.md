[![PHP from Packagist](https://img.shields.io/packagist/php-v/kiwa/amp)](http://www.php.net)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/fac6de235bbb49e897072cc0a8665946)](https://www.codacy.com/manual/wirbelwild/Kiwa-Sitemap?utm_source=tobiaskoengeter@bitbucket.org&amp;utm_medium=referral&amp;utm_content=wirbelwild/kiwa-sitemap&amp;utm_campaign=Badge_Grade)
[![Latest Stable Version](https://poser.pugx.org/kiwa/amp/v/stable)](https://packagist.org/packages/kiwa/amp)
[![Total Downloads](https://poser.pugx.org/kiwa/amp/downloads)](https://packagist.org/packages/kiwa/amp)
[![License](https://poser.pugx.org/kiwa/amp/license)](https://packagist.org/packages/kiwa/amp)

# Kiwa AMP

Creates an AMP version of a page. 

This library is meant to run along with [Kiwa](https://www.kiwa.io) but don't has to.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/kiwa/amp). Add it to your project by running `$ composer require kiwa/amp`. 

If you are using the [Kiwa Console](https://packagist.org/packages/kiwa/console) some commands will be available then.

## Usage 

__Please note that this library is under development!__

## Help 

If you have questions about Kiwa feel free to contact us under `hello@kiwa.io`.