<?php


use Lullabot\AMP\AMP;
use Lullabot\AMP\Validate\Scope;

require '../vendor/autoload.php';

$htmlRaw = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'sample'.DIRECTORY_SEPARATOR.'hello.html');

$amp = new AMP();
$amp->loadHtml($htmlRaw, ['scope' => Scope::HTML_SCOPE]);

$htmlConverted = $amp->convertToAmpHtml();

file_put_contents(
    __DIR__.DIRECTORY_SEPARATOR.'sample'.DIRECTORY_SEPARATOR.'hello-amp.html',
    $htmlConverted
);

print_r($amp->warningsHumanText());